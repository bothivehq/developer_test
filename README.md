![alt text](https://i.imgur.com/7viJNOe.png "Logo Bothive")

# Developer test

Scope of test:

Practical test (max. 2hr test)

## 1. Theoretical test
PDF file `theoretical_test.pdf` found in the root of this directory. To be completed whilst in videocall with us.
Complete without consulting a computer, the main objective is gaining insight in how you think & communicate while solving these challenges.

## 2. Practical test
Make a web application that interacts with the Star Wars API (https://swapi.dev/). 
When a user goes to your website, they should see an overview of all the planets. 
When they click on a planet they should get more information about that planet. 
If there are any notable people from the planet your application should also display some basic information about them on the “Planet Details” page. 
For the coding challenge you’re free to use any programming language or framework you’d like.

Please contact us when you have questions! Have fun!